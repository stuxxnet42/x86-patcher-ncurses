#include "Hackable_function.hpp"

/*
 * updates permissions for a page (linux specific)
 */
void make_writeable(asm_sequence_view sq){ //should check size of sq here...
    uintptr_t function_p = reinterpret_cast<uintptr_t>(sq.ptr);
    const size_t PAGE_SIZE = 4096;
    uintptr_t page_base = function_p-(function_p % PAGE_SIZE);
    if (mprotect(reinterpret_cast<void*>(page_base), 1, PROT_READ|PROT_WRITE|PROT_EXEC) < 0) {
        throw std::runtime_error("could not change permissions");
    }
}


void modify_function(asm_sequence_view function, std::vector<uint8_t>& patch, bool padding){
    if(function.size < patch.size()){
        throw std::runtime_error("patch bigger than function");
    }
    if(padding){
        size_t no_padding_nops = function.size - patch.size();
        for(size_t i=0; i<no_padding_nops; i++){
            patch.push_back(0x90);
        }
    }
    for(size_t i=0; i<patch.size(); i++){
        function.ptr[i] = patch[i];
    }
}

std::string gen_str_marker(uint8_t index, uint8_t magic_byte){
    std::stringstream ss;
    ss << "nop dword ptr [rax + rax + 0x"
       << std::hex << int(index)
       << "0000"
       << int(magic_byte)
       << "]";
    return ss.str();
}





Disassembler::Disassembler(){
    if(cs_open(CS_ARCH_X86, CS_MODE_64, &handle) != CS_ERR_OK){
        throw std::runtime_error("couldn't init capstone");
    }
}

std::string Disassembler::disassemble_sequence(asm_sequence_view sq){
    size_t count;
    count = cs_disasm(handle, sq.ptr, sq.size, 0, 0, &insn);
    std::string result;
    for(size_t i=0; i<count; i++){
        result += std::string(insn[i].mnemonic) + ' ' + std::string(insn[i].op_str) + '\n';
    }
    cs_free(insn, count);
    return result;
}

std::optional<size_t> Disassembler::find_marker(asm_sequence_view sq, const std::string& marker){
    const uint8_t* ptr = sq.ptr;
    size_t size = sq.size;
    size_t address = 0;
    insn = cs_malloc(handle);
    size_t offset = 0;
    while(cs_disasm_iter(handle, &ptr, &size, &address, insn)) {
        std::string result;
        result += std::string(insn->mnemonic) + " " + std::string(insn->op_str);
        
        offset += insn->size;
        if(result == marker){
            cs_free(insn, 1);
            return offset;
        }
    }
    cs_free(insn, 1);
    return {};
}

std::optional<asm_sequence_view>
Disassembler::find_sequence(   asm_sequence_view sq, 
                                    const std::string& marker1,
                                    const std::string& marker2){
    
    auto begin = find_marker(sq, marker1);
    auto end = find_marker(sq, marker2);
    if(!begin or !end){
        return {};
    }
    *end -= 8;
    if(*begin >= *end){
        return {};
    }
    return {{sq.ptr + *begin, *end - *begin}};
}





assembler::assembler(){
    if(ks_open(KS_ARCH_X86, KS_MODE_64, &ks) != KS_ERR_OK){
        throw std::runtime_error("failed to initialize keystone");
    }
}
assembler::~assembler(){
    ks_close(ks);
}
std::optional<std::vector<uint8_t>> assembler::assemble(const std::string& code){
    uint8_t* payload;
    size_t size;
    size_t count;
    if(ks_asm(ks, code.c_str(), 0, &payload, &size, &count) != KS_ERR_OK){
        std::cout<<"failed to assemble code\n";
        return {};
    }
    std::vector<uint8_t> result(payload, payload+size);
    ks_free(payload);
    return result;
}

void replace_all(std::string& str, const std::string& from, const std::string& to) {
    if(from.empty())
        return;
    size_t start_pos = 0;
    while((start_pos = str.find(from, start_pos)) != std::string::npos) {
        str.replace(start_pos, from.length(), to);
        start_pos += to.length();
    }
}

Hackable_function::Hackable_function(){
    whole_function = {reinterpret_cast<void*>(foo), FOOSIZE};
    make_writeable(whole_function);
    whole_function_str = disas.disassemble_sequence(whole_function);
    
    for(size_t i = 1;; i++){
        auto seq = disas.find_sequence(whole_function, gen_str_marker(2*i-1), gen_str_marker(2*i));
        if(seq){
            hackable_sequences.push_back(*seq);
        }
        else{
            break;
        }
    }
    
    if(hackable_sequences.size() == 0){
        throw std::runtime_error("no hackable sequences found");
    }
    
    for(size_t i = 0; i < hackable_sequences.size(); i++){
        std::string sequence_str = disas.disassemble_sequence(hackable_sequences[i]);
        replace_all(sequence_str, "nop \n", "");
        hackable_sequences_str.push_back(std::move(sequence_str));
    }
}


bool Hackable_function::patch_sequence(size_t index, std::string& new_code){
    std::replace(new_code.begin(), new_code.end(), '\n', ';');
    new_code += ';';
    auto bytes = as.assemble(new_code);
    if(bytes->size() <= hackable_sequences[index].size){
        modify_function(hackable_sequences[index], *bytes);
        hackable_sequences_str[index] = disas.disassemble_sequence(hackable_sequences[index]);
        whole_function_str = disas.disassemble_sequence(whole_function);
        return true;
    }
    return false;
}













