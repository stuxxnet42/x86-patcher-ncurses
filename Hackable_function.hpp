#ifndef Disassembler_HPP
#define Disassembler_HPP

#include <iostream>
#include <utility>
#include <string>
#include <sstream>
#include <iomanip>
#include <vector>
#include <optional>
#include <stdexcept>
#include <algorithm>
#include <capstone/capstone.h>
#include <keystone/keystone.h>
#include <sys/mman.h>
#include "test_function.hpp"

struct asm_sequence_view{
    uint8_t* ptr;
    size_t size;
    asm_sequence_view(void* v_ptr, size_t size) :
        ptr(reinterpret_cast<uint8_t*>(v_ptr)),
        size(size) {}
    asm_sequence_view(){
        ptr = nullptr;
        size=0;
    }
};


void make_writeable(asm_sequence_view sq);

void modify_function(asm_sequence_view function, std::vector<uint8_t>& patch, bool padding=true);

std::string gen_str_marker(uint8_t index, uint8_t magic_byte = 0x42);

class Disassembler{
    csh handle;
    cs_insn *insn;
public:
    Disassembler();
    
    std::string disassemble_sequence(asm_sequence_view sq);
    
    std::optional<size_t> find_marker(asm_sequence_view sq, const std::string& marker);
    
    std::optional<asm_sequence_view> find_sequence( asm_sequence_view sq, 
                                                    const std::string& marker1,
                                                    const std::string& marker2);
};



class assembler{
    ks_engine *ks;
public:
    assembler();
    ~assembler();
    std::optional<std::vector<uint8_t>> assemble(const std::string& code);
    
};

void replace_all(std::string& str, const std::string& from, const std::string& to);

class Hackable_function{
public:
    std::string whole_function_str;
    std::vector<std::string> hackable_sequences_str;
    
    Disassembler disas;
    asm_sequence_view whole_function;
    std::vector<asm_sequence_view> hackable_sequences;
    
    assembler as;
    
    std::string file_test_str;
    
    Hackable_function();
    bool patch_sequence(size_t index, std::string& new_code);
    
};














#endif
