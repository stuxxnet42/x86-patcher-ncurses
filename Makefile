CC=g++
CFLAGS=-g -c -O2 -Wall -std=c++17
LDFLAGS=-lncurses -lkeystone -lstdc++ -lm -lcapstone
SOURCES=main.cpp  UI_manager_nc.cpp Hackable_function.cpp
F_SOURCES=test_function.cpp
OBJECTS=$(SOURCES:.cpp=.o)
F_OBJECTS=$(F_SOURCES:.cpp=.o)
EXECUTABLE=run.out

all: $(SOURCES) $(EXECUTABLE) $(HIGHLIGHTS)
    
$(EXECUTABLE): $(OBJECTS)
	$(CC) $(OBJECTS) $(F_OBJECTS) -o $@ $(LDFLAGS)

$(OBJECTS): %.o: %.cpp $(F_OBJECTS)
	$(CC) $(CFLAGS) -D FOOSIZE=$(shell ./get_function_size.sh) $< -o $@

$(F_OBJECTS): %.o: %.cpp
	$(CC) $(CFLAGS) $< -o $@

clean:
	rm $(OBJECTS) $(F_OBJECTS) $(HIGHLIGHTS)
