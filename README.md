# x86 patcher ncurses

requirements: ncurses, capstone, keystone

usage: run in maximized terminal, hit enter, switch highlighted window with TAB, clear window with F6, patch code and re-run with F5

screenshot:

![screenshot](screenshot.png?raw=true "screenshot")
