#include "UI_manager_nc.hpp"

#include <fstream>

curses_window::curses_window(int xpos, int ypos, int xsize, int ysize){
    win_size = {xsize, ysize};
    win = newwin(ysize, xsize, ypos, xpos);
    border_win = newwin(ysize+2, xsize+2, ypos-1, xpos-1);
    wbkgd(win, COLOR_PAIR(1));
    wbkgd(border_win, COLOR_PAIR(1));
    box(border_win, 0, 0);
    wrefresh(border_win);
    wrefresh(win);
}

curses_window::curses_window(curses_window&& other){
    win = other.win;
    border_win = other.border_win;
    win_size = other.win_size;
    other.win = nullptr;
    other.border_win = nullptr;
}

curses_window::~curses_window(){
    delwin(win);
    delwin(border_win);
}

void curses_window::addchr(int ch){
    waddch(win, ch);
}

void curses_window::delchr(){
    int x, y;
    getyx(win, y, x);
    if(x>0){
        x--;
        wmove(win, y, x);
        wdelch(win);
    }
    else{
        if(y>0){
            y--;
            x = win_size.first - 1;
            wmove(win, y, x);
            wdelch(win);
        }
    }
    
}

void curses_window::clear(){
    wmove(win, 0, 0);
    werase(win);
}

void curses_window::set_string(const std::string& str){
    wmove(win, 0, 0);
    waddstr(win, str.c_str());
}

void curses_window::render(){
    wrefresh(border_win);
    wrefresh(win);
}

std::string curses_window::get_text(){
    int no_chars = win_size.first * win_size.second;
    std::vector<unsigned int> data(no_chars+5);
    int written_chars = 0;
    for(int y = 0; y < win_size.second; y++){
        written_chars += mvwinchnstr(win, y, 0, data.data() + written_chars, data.size() - written_chars);
        data[written_chars] = '\n';
        written_chars++;
    }
    std::string result(data.size(), ' ');
    for(size_t i = 0; i < data.size(); i++){
        result[i] = (data[i] & A_CHARTEXT);
    }
    return result;
}

void curses_window::up(){
    int x, y;
    getyx(win, y, x);
    if(y > 0){
        y--;
        wmove(win, y, x);
    }
}

void curses_window::down(){
    int x, y;
    getyx(win, y, x);
    if(y < win_size.second){
        y++;
        wmove(win, y, x);
    }
}

void curses_window::left(){
    int x, y;
    getyx(win, y, x);
    if(x > 0){
        x--;
        wmove(win, y, x);
    }
}

void curses_window::right(){
    int x, y;
    getyx(win, y, x);
    if(x < win_size.first){
        x++;
        wmove(win, y, x);
    }
}

UI_manager_nc::UI_manager_nc(std::string comments) : function_control_win(2, 2, 30, 10),
                                                     cpp_source_win(68, 2, 80, 40),
                                                     disas_win(151, 2, 40, 40)
{
    disas_win.set_string("disas win");
    int i=0;
    for(const auto& s : hf.hackable_sequences_str){
        input_buffers_win.emplace_back(curses_window(35, 13*(i++)+2, 30, 10));
        input_buffers_win.back().set_string(s);
    }
    
    std::ifstream is("test_function.cpp");
    while(true){
        std::string line;
        std::getline(is, line);
        cpp_source_code += line + "\n";
        if(is.eof()){
            break;
        }
    }
    disas_win.set_string(hf.whole_function_str.c_str());
    function_result = foo(function_data);
    cpp_source_win.set_string(cpp_source_code);
    std::string f_control_str;
    for(auto i : function_data){
        f_control_str += std::to_string(i) + '\n';
    }
    f_control_str += "\n\nresult: " + std::to_string(function_result);
    function_control_win.set_string(f_control_str);
}

bool UI_manager_nc::render_all(int ch){
    bool modified_asm = false;
    if(ch == 27){
        return true;
    }
    if((isascii(ch) and isprint(ch)) or ch == '\n'){
        input_buffers_win[window_index].addchr(ch);
    }
    else{
        if(ch == '\t'){
            window_index = (window_index + 1) % input_buffers_win.size();
        }
        if(ch == 127){
            input_buffers_win[window_index].delchr();
        }
        if(ch == KEY_F(5)){
            modified_asm = true;
            for(size_t i = 0; i < input_buffers_win.size(); i++){
                std::string new_code = input_buffers_win[i].get_text();
                hf.patch_sequence(i, new_code);
            }
        }
        if(ch == KEY_F(6)){
            input_buffers_win[window_index].clear();
        }
        if(ch == KEY_UP){
            input_buffers_win[window_index].up();
        }
        if(ch == KEY_DOWN){
            input_buffers_win[window_index].down();
        }
        if(ch == KEY_LEFT){
            input_buffers_win[window_index].left();
        }
        if(ch == KEY_RIGHT){
            input_buffers_win[window_index].right();
        }
    }
    for(auto& i : input_buffers_win){
        i.render();
    }
    if(modified_asm){
        function_result = foo(function_data);
        disas_win.set_string(hf.whole_function_str.c_str());
        std::string f_control_str;
        for(auto i : function_data){
            f_control_str += std::to_string(i) + '\n';
        }
        f_control_str += "\n\nresult: " + std::to_string(function_result);
        function_control_win.set_string(f_control_str);
    }
    function_control_win.render();
    cpp_source_win.render();
    disas_win.render();
    input_buffers_win[window_index].render();
    return false;
}

