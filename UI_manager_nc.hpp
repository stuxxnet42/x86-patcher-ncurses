#ifndef UI_MANAGER_NC_HPP
#define UI_MANAGER_NC_HPP

#include <vector>
#include <string>

#include <curses.h>

#include "Hackable_function.hpp"
#include "test_function.hpp"


class curses_window{
    WINDOW* win;
    WINDOW* border_win;
    
    
public:
    std::pair<int, int> win_size;
    curses_window(int xpos, int ypos, int xsize, int ysize);
    curses_window(const curses_window& other) = delete;
    curses_window(curses_window&& other);
    ~curses_window();
    void addchr(int ch);
    void delchr();
    void clear();
    void set_string(const std::string& str);
    void render();
    std::string get_text();
    void up();
    void down();
    void left();
    void right();
};

class UI_manager_nc{
    curses_window function_control_win;
    curses_window cpp_source_win;
    curses_window disas_win;
    
    std::vector<curses_window> input_buffers_win;
    
    int window_index = 0;
    
    Hackable_function hf;
    std::vector<int> function_data = {1, 2, 3, 4};
    int function_result = 0;
    bool auto_run_function = true;
    std::string cpp_source_code;
    
public:
    UI_manager_nc(std::string comments = "comments go here");
    bool render_all(int ch);
};

#endif
