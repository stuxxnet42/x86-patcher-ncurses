#include <vector>
#include <string>
#include <iostream>
#include <stdexcept>

#include "UI_manager_nc.hpp"


void init(){
    initscr();
    cbreak();
    noecho();
    keypad(stdscr, TRUE);
    start_color();
    init_pair(1, COLOR_WHITE, COLOR_BLUE);
    getch();
}

void run(){
    UI_manager_nc manager;
    manager.render_all(0);
    bool done = false;
    while(!done){
        int ch = getch();
        done = manager.render_all(ch);
    }
}


int main(){
    init();
    run();
    endwin();
}
