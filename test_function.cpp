#include "test_function.hpp"


int __attribute__ ((noinline)) foo(std::vector<int>& data){
    ASSEMBLY_MARKER(0x1)
    int sum=0;
    INSERT_NOP
    INSERT_NOP
    CLOBBER_VAR(sum)
    ASSEMBLY_MARKER(0x2)
    for(auto i : data){
        ASSEMBLY_MARKER(0x3)
        if(i>3){
            sum++;
        }
        INSERT_NOP
        INSERT_NOP
        ASSEMBLY_MARKER(0x4)
    }
    return sum;
}
