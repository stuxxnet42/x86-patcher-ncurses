#ifndef TEST_FUNCTION_HPP
#define TEST_FUNCTION_HPP

#include <vector>
#define _MAKE_HEX(x) #x
#define MAKE_HEX(x) _MAKE_HEX(x)
//#define ASSEMBLY_MARKER(x) asm volatile(".byte 0x0F, 0x1F, 0x84, 0x00, 0x42, 0x00, 0x00, " MAKE_HEX(x) ::: "memory"); //never use 0x00 as a marker!
#define ASSEMBLY_MARKER(x) asm volatile(".byte 0x0F, 0x1F, 0x84, 0x00, 0x42, 0x00, 0x00, " MAKE_HEX(x));
#define CLOBBER_VAR(x) asm volatile("":"+r" (x)::);
#define INSERT_NOP asm volatile(".byte 0x90");

int __attribute__ ((noinline)) foo(std::vector<int>& data);


#endif
